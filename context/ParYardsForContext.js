import { createContext, useState } from 'react';


export const ParYardsContext = createContext();

const ParYardsProvider = ({ children }) => {

  const [hole1_p, setpar_1] = useState('')
  const [hole1_y, setyards_1] = useState('') 

  const [hole2_p, setpar_2] = useState('')
  const [hole2_y, setyards_2] = useState('')

  const [hole3_p, setpar_3] = useState('')
  const [hole3_y, setyards_3] = useState('')

  const [hole4_p, setpar_4] = useState('') 
  const [hole4_y, setyards_4] = useState('')

  const [hole5_p, setpar_5] = useState('')   
  const [hole5_y, setyards_5] = useState('')

  const [hole6_p, setpar_6] = useState('') 
  const [hole6_y, setyards_6] = useState('')

  const [hole7_p, setpar_7] = useState('')
  const [hole7_y, setyards_7] = useState('')

  const [hole8_p, setpar_8] = useState('') 
  const [hole8_y, setyards_8] = useState('') 

  const [hole9_p, setpar_9] = useState('')
  const [hole9_y, setyards_9] = useState('')
  
  const [hole10_p, setpar_10] = useState('')
  const [hole10_y, setyards_10] = useState('') 

  const [hole11_p, setpar_11] = useState('')
  const [hole11_y, setyards_11] = useState('')

  const [hole12_p, setpar_12] = useState('')
  const [hole12_y, setyards_12] = useState('')

  const [hole13_p, setpar_13] = useState('') 
  const [hole13_y, setyards_13] = useState('')

  const [hole14_p, setpar_14] = useState('')   
  const [hole14_y, setyards_14] = useState('')

  const [hole15_p, setpar_15] = useState('') 
  const [hole15_y, setyards_15] = useState('')

  const [hole16_p, setpar_16] = useState('')
  const [hole16_y, setyards_16] = useState('')

  const [hole17_p, setpar_17] = useState('') 
  const [hole17_y, setyards_17] = useState('') 

  const [hole18_p, setpar_18] = useState('')
  const [hole18_y, setyards_18] = useState('') 

    const setParYards = {
      hole1_p, setpar_1, hole1_y, setyards_1, hole2_p, setpar_2, hole2_y, setyards_2,
      hole3_p, setpar_3, hole3_y, setyards_3, hole4_p, setpar_4, hole4_y, setyards_4,
      hole5_p, setpar_5, hole5_y, setyards_5, hole6_p, setpar_6, hole6_y, setyards_6,
      hole7_p, setpar_7, hole7_y, setyards_7, hole8_p, setpar_8, hole8_y, setyards_8, 
      hole9_p, setpar_9, hole9_y, setyards_9, hole10_p, setpar_10, hole10_y, setyards_10, 
      hole11_p, setpar_11, hole11_y, setyards_11, hole12_p, setpar_12, hole12_y, setyards_12,
      hole13_p, setpar_13, hole13_y, setyards_13, hole14_p, setpar_14, hole14_y, setyards_14,
      hole15_p, setpar_15, hole15_y, setyards_15, hole16_p, setpar_16, hole16_y, setyards_16,
      hole17_p, setpar_17, hole17_y, setyards_17, hole18_p, setpar_18, hole18_y, setyards_18
    }

  return (
    <ParYardsContext.Provider value={setParYards}>
      {children}
    </ParYardsContext.Provider>
  );
};

export default ParYardsProvider