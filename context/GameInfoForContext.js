import { createContext, useState } from 'react';


export const GameContext = createContext();

const GameProvider = ({ children }) => {
  
    const [email, setEmail] = useState('');
    const [course, setCourse] = useState('');
    const [player1, setplayer1] = useState('')
    const [player2, setplayer2] = useState('')
    const [player3, setplayer3] = useState('')
    const [player4, setplayer4] = useState('')

    const [holeTotal_1, setholeTotal_1] = useState(null)
    const [holeTotal_2, setholeTotal_2] = useState(null)  
    const [holeTotal_3, setholeTotal_3] = useState(null) 
    const [holeTotal_4, setholeTotal_4] = useState(null) 

    const [holeTotalb_1, setholeTotalb_1] = useState(null)
    const [holeTotalb_2, setholeTotalb_2] = useState(null)  
    const [holeTotalb_3, setholeTotalb_3] = useState(null) 
    const [holeTotalb_4, setholeTotalb_4] = useState(null) 
    
    const [slope_1, setSlope1] = useState(null)
    const [rate_1, setRate1] = useState(null)  
    const [slope_2, setSlope2] = useState(null)
    const [rate_2, setRate2] = useState(null)
    const [slope_3, setSlope3] = useState(null)
    const [rate_3, setRate3] = useState(null)  
    const [slope_4, setSlope4] = useState(null)
    const [rate_4, setRate4] = useState(null)

    const [index1, setindex1] = useState(null)
    const [index2, setindex2] = useState(null)  
    const [index3, setindex3] = useState(null)
    const [index4, setindex4] = useState(null)

    const [par1, setpar1] = useState(null)
    const [par2, setpar2] = useState(null)  
    const [par3, setpar3] = useState(null)
    const [par4, setpar4] = useState(null)
    // const [startDate, setStartDate] = useState(new Date());

    const setValues = {
      email, setEmail, course, setCourse, player1, setplayer1, 
      player2, setplayer2, player3, setplayer3, player4, setplayer4,
      holeTotal_1, setholeTotal_1, holeTotal_2, setholeTotal_2,
      holeTotal_3, setholeTotal_3, holeTotal_4, setholeTotal_4,
      holeTotalb_1, setholeTotalb_1, holeTotalb_2, setholeTotalb_2,
      holeTotalb_3, setholeTotalb_3, holeTotalb_4, setholeTotalb_4,
      slope_1, setSlope1, rate_1, setRate1, slope_2, setSlope2, rate_2, setRate2,
      slope_3, setSlope3, rate_3, setRate3, slope_4, setSlope4, rate_4, setRate4,
      index1, setindex1, index2, setindex2, index3, setindex3, index4, setindex4,
      par1, setpar1, par2, setpar2, par3, setpar3, par4, setpar4
    }

  return (
    <GameContext.Provider value={setValues}>
      {children}
    </GameContext.Provider>
  );
};

export default GameProvider