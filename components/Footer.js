import React from 'react'
import styles from '../styles/Home.module.css'

export default function Footer() {
    return (
        <div>
            <p className={styles.footerxlix}>
                © {new Date().getFullYear()}
                <a href="https://www.xlixelit.com">
                    @ xlixelit.com
                </a>
            </p>
        </div>
    )
}




