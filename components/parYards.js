import React, { useContext } from 'react'
import styles from '../styles/parYards.module.css'
import { ParYardsContext }  from '../context/ParYardsForContext'

export default function ParYards() {

    const {hole1_p, setpar_1, hole1_y, setyards_1, hole2_p, setpar_2, hole2_y, setyards_2,
        hole3_p, setpar_3, hole3_y, setyards_3, hole4_p, setpar_4, hole4_y, setyards_4,
        hole5_p, setpar_5, hole5_y, setyards_5, hole6_p, setpar_6, hole6_y, setyards_6,
        hole7_p, setpar_7, hole7_y, setyards_7, hole8_p, setpar_8, hole8_y, setyards_8, 
        hole9_p, setpar_9, hole9_y, setyards_9} = useContext(ParYardsContext);



    return (
    <div className={styles.top_div}>
    <div className={styles.parYards_div}>
        <p className={styles.hole_options}>Optional: input par and yardage.</p>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number1}>Hole</p>
            </div>
            <div className={styles.col}>
                <p>Par</p>
            </div>
            <div className={styles.col}>
                <p>Yards</p>
            </div>
        </div> 
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number1}>1</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole1_p"
                type="number"
                onChange={(event) => setpar_1(event.target.value)}
                value={hole1_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole1_y"
                type="number"
                onChange={(event) => setyards_1(event.target.value)}
                value={hole1_y}
                className={styles.hole_input}
                />
            </div>
        </div>    
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>2</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole2_p"
                type="number"
                onChange={(event) => setpar_2(event.target.value)}
                value={hole2_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole2_y"
                type="number"
                onChange={(event) => setyards_2(event.target.value)}
                value={hole2_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>3</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole3_p"
                type="number"
                onChange={(event) => setpar_3(event.target.value)}
                value={hole3_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole3_y"
                type="number"
                onChange={(event) => setyards_3(event.target.value)}
                value={hole3_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>4</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole4_p"
                type="number"
                onChange={(event) => setpar_4(event.target.value)}
                value={hole4_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole4_y"
                type="number"
                onChange={(event) => setyards_4(event.target.value)}
                value={hole4_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>5</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole5_p"
                type="number"
                onChange={(event) => setpar_5(event.target.value)}
                value={hole5_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole5_y"
                type="number"
                onChange={(event) => setyards_5(event.target.value)}
                value={hole5_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>6</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole6_p"
                type="number"
                onChange={(event) => setpar_6(event.target.value)}
                value={hole6_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole6_y"
                type="number"
                onChange={(event) => setyards_6(event.target.value)}
                value={hole6_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>7</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole7_p"
                type="number"
                onChange={(event) => setpar_7(event.target.value)}
                value={hole7_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole7_y"
                type="number"
                onChange={(event) => setyards_7(event.target.value)}
                value={hole7_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>8</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole8_p"
                type="number"
                onChange={(event) => setpar_8(event.target.value)}
                value={hole8_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole8_y"
                type="number"
                onChange={(event) => setyards_8(event.target.value)}
                value={hole8_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number9}>9</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole9_p"
                type="number"
                onChange={(event) => setpar_9(event.target.value)}
                value={hole9_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole9_y"
                type="number"
                onChange={(event) => setyards_9(event.target.value)}
                value={hole9_y}
                className={styles.hole_input}
                />
            </div>
        </div>
    </div>
    </div>
    )
}
