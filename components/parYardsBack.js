import React, { useContext } from 'react'
import styles from '../styles/parYards.module.css'
import { ParYardsContext }  from '../context/ParYardsForContext'

export default function ParYardsBack() {

    const {hole10_p, setpar_10, hole10_y, setyards_10, 
        hole11_p, setpar_11, hole11_y, setyards_11, hole12_p, setpar_12, hole12_y, setyards_12,
        hole13_p, setpar_13, hole13_y, setyards_13, hole14_p, setpar_14, hole14_y, setyards_14,
        hole15_p, setpar_15, hole15_y, setyards_15, hole16_p, setpar_16, hole16_y, setyards_16,
        hole17_p, setpar_17, hole17_y, setyards_17, hole18_p, setpar_18, hole18_y, setyards_18
        } = useContext(ParYardsContext);


    return (
    <div className={styles.top_div}>
    <div className={styles.parYards_div}>
        <p className={styles.hole_options}></p>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number1}>Hole</p>
            </div>
            <div className={styles.col}>
                <p>Par</p>
            </div>
            <div className={styles.col}>
                <p>Yards</p>
            </div>
        </div> 
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number1}>10</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole10_p"
                type="number"
                onChange={(event) => setpar_10(event.target.value)}
                value={hole10_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole10_y"
                type="number"
                onChange={(event) => setyards_10(event.target.value)}
                value={hole10_y}
                className={styles.hole_input}
                />
            </div>
        </div>    
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>11</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole11_p"
                type="number"
                onChange={(event) => setpar_11(event.target.value)}
                value={hole11_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole11_y"
                type="number"
                onChange={(event) => setyards_11(event.target.value)}
                value={hole11_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>12</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole12_p"
                type="number"
                onChange={(event) => setpar_12(event.target.value)}
                value={hole12_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole12_y"
                type="number"
                onChange={(event) => setyards_12(event.target.value)}
                value={hole12_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>13</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole13_p"
                type="number"
                onChange={(event) => setpar_13(event.target.value)}
                value={hole13_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole13_y"
                type="number"
                onChange={(event) => setyards_13(event.target.value)}
                value={hole13_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>14</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole14_p"
                type="number"
                onChange={(event) => setpar_14(event.target.value)}
                value={hole14_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole14_y"
                type="number"
                onChange={(event) => setyards_14(event.target.value)}
                value={hole14_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>15</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole15_p"
                type="number"
                onChange={(event) => setpar_15(event.target.value)}
                value={hole15_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole15_y"
                type="number"
                onChange={(event) => setyards_15(event.target.value)}
                value={hole15_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>16</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole16_p"
                type="number"
                onChange={(event) => setpar_16(event.target.value)}
                value={hole16_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole16_y"
                type="number"
                onChange={(event) => setyards_16(event.target.value)}
                value={hole16_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number}>17</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole17_p"
                type="number"
                onChange={(event) => setpar_17(event.target.value)}
                value={hole17_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole17_y"
                type="number"
                onChange={(event) => setyards_17(event.target.value)}
                value={hole17_y}
                className={styles.hole_input}
                />
            </div>
        </div>
        <div className={styles.row}>
            <div className={styles.col}>
                <p className={styles.hole_number9}>18</p>
            </div>
            <div className={styles.col}>
                <input
                name="hole18_p"
                type="number"
                onChange={(event) => setpar_18(event.target.value)}
                value={hole18_p}
                className={styles.hole_input}
                />
            </div>
            <div className={styles.col}>
                <input
                name="hole18_y"
                type="number"
                onChange={(event) => setyards_18(event.target.value)}
                value={hole18_y}
                className={styles.hole_input}
                />
            </div>
        </div>
    </div>
    </div>
    )
}