import React, { useState, useEffect, useContext } from 'react'
import styles from '../styles/Scorecardcom.module.css'
import { GameContext }  from '../context/GameInfoForContext'
import { ParYardsContext }  from '../context/ParYardsForContext'

export default function Scorecardcom() {

    const {course, player1, player2, player3, player4, 
        holeTotal_1, setholeTotal_1, holeTotal_2, setholeTotal_2,
        holeTotal_3, setholeTotal_3, holeTotal_4, setholeTotal_4} = useContext(GameContext);

    const {hole1_p, hole1_y, hole2_p, hole2_y,
        hole3_p, hole3_y, hole4_p, hole4_y,
        hole5_y, hole5_p, hole6_y, hole6_p,
        hole7_y, hole7_p, hole8_y, hole8_p, 
        hole9_p, hole9_y} = useContext(ParYardsContext); 

    const [hole1_1, sethole1_1] = useState(null)
    const [hole1_2, sethole1_2] = useState(null)  
    const [hole1_3, sethole1_3] = useState(null) 
    const [hole1_4, sethole1_4] = useState(null) 

    const [hole2_1, sethole2_1] = useState(null)
    const [hole2_2, sethole2_2] = useState(null) 
    const [hole2_3, sethole2_3] = useState(null)
    const [hole2_4, sethole2_4] = useState(null)

    const [hole3_1, sethole3_1] = useState(null)
    const [hole3_2, sethole3_2] = useState(null)  
    const [hole3_3, sethole3_3] = useState(null) 
    const [hole3_4, sethole3_4] = useState(null) 

    const [hole4_1, sethole4_1] = useState(null)
    const [hole4_2, sethole4_2] = useState(null)  
    const [hole4_3, sethole4_3] = useState(null) 
    const [hole4_4, sethole4_4] = useState(null) 

    const [hole5_1, sethole5_1] = useState(null)
    const [hole5_2, sethole5_2] = useState(null)  
    const [hole5_3, sethole5_3] = useState(null) 
    const [hole5_4, sethole5_4] = useState(null) 

    const [hole6_1, sethole6_1] = useState(null)
    const [hole6_2, sethole6_2] = useState(null)  
    const [hole6_3, sethole6_3] = useState(null) 
    const [hole6_4, sethole6_4] = useState(null) 

    const [hole7_1, sethole7_1] = useState(null)
    const [hole7_2, sethole7_2] = useState(null)  
    const [hole7_3, sethole7_3] = useState(null) 
    const [hole7_4, sethole7_4] = useState(null) 

    const [hole8_1, sethole8_1] = useState(null)
    const [hole8_2, sethole8_2] = useState(null)  
    const [hole8_3, sethole8_3] = useState(null) 
    const [hole8_4, sethole8_4] = useState(null) 

    const [hole9_1, sethole9_1] = useState(null)
    const [hole9_2, sethole9_2] = useState(null)  
    const [hole9_3, sethole9_3] = useState(null) 
    const [hole9_4, sethole9_4] = useState(null) 

    // const [holeTotal_1, setholeTotal_1] = useState(null)
    // const [holeTotal_2, setholeTotal_2] = useState(null)  
    // const [holeTotal_3, setholeTotal_3] = useState(null) 
    // const [holeTotal_4, setholeTotal_4] = useState(null) 

    var totalStrokes_1 = {hole1_1, hole2_1, hole3_1, hole4_1, hole5_1, hole6_1, hole7_1, hole8_1, hole9_1}
    var totalStrokes_2 = {hole1_2, hole2_2, hole3_2, hole4_2, hole5_2, hole6_2, hole7_2, hole8_2, hole9_2}
    var totalStrokes_3 = {hole1_3, hole2_3, hole3_3, hole4_3, hole5_3, hole6_3, hole7_3, hole8_3, hole9_3}
    var totalStrokes_4 = {hole1_4, hole2_4, hole3_4, hole4_4, hole5_4, hole6_4, hole7_4, hole8_4, hole9_4}

     


    useEffect(() => {
        setholeTotal_1(Number(hole1_1) + Number(hole2_1) + Number(hole3_1) + Number(hole4_1) + Number(hole5_1) + Number(hole6_1) + Number(hole7_1) + Number(hole8_1) + Number(hole9_1));
    }, [hole1_1, hole2_1, hole3_1, hole4_1, hole5_1, hole6_1, hole7_1, hole8_1, hole9_1, setholeTotal_1])

    useEffect(() => {
        setholeTotal_2(Number(hole1_2) + Number(hole2_2) + Number(hole3_2) + Number(hole4_2) + Number(hole6_2) + Number(hole7_2) + Number(hole8_2) + Number(hole9_2));
    }, [hole1_2, hole2_2, hole3_2, hole4_2, hole5_2, hole6_2, hole7_2, hole8_2, hole9_2, setholeTotal_2])

    useEffect(() => {
        setholeTotal_3(Number(hole1_3) + Number(hole2_3) + Number(hole3_3) + Number(hole4_3) + Number(hole5_3) + Number(hole6_3) + Number(hole7_3) + Number(hole8_3) + Number(hole9_3));
    }, [hole1_3, hole2_3, hole3_3, hole4_3, hole5_3, hole6_3, hole7_3, hole8_3, hole9_3, setholeTotal_3])

    useEffect(() => {
        setholeTotal_4(Number(hole1_4) + Number(hole2_4) + Number(hole3_4) + Number(hole4_4) + Number(hole5_4) + Number(hole6_4) + Number(hole7_4) + Number(hole8_4) + Number(hole9_4));
    }, [hole1_4, hole2_4, hole3_4, hole4_4, hole5_4, hole6_4, hole7_4, hole8_4, hole9_4, setholeTotal_4])
// jscharlach@hotmail.com

    return (
        <div>
            <div className={styles.course_name_wrapper}>
            <h2 className={styles.course_name}>{course}</h2>
            <h2 className={styles.front_back}>Front Nine</h2>
            </div> 
            <div className={styles.curvedbit}></div>
            <div className={styles.curvedbit2}></div>
            <div className={styles.innercard_wrapper}>
                <div className={styles.innercard}>
                    <div className={styles.hole_name}>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <div className={styles.row}>
                                    <div className={styles.col2}>
                                        <p>H</p>&nbsp;&nbsp;
                                    </div>    
                                    <div className={styles.col2}>   
                                        <span className={styles.par}>Par</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player1}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player2}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player3}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player4}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number1}>1<span className={styles.spanpar}></span>{hole1_p}</p>
                                {/* <p className={styles.hole_number1}>{hole1_y}</p> */}
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole1_1"
                                type="number"
                                onChange={(event) => sethole1_1(event.target.value)}
                                value={hole1_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole1_2"
                                type="number"
                                onChange={(event) => sethole1_2(event.target.value)}
                                value={hole1_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole1_3"
                                type="number"
                                onChange={(event) => sethole1_3(event.target.value)}
                                value={hole1_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole1_4"
                                type="number"
                                onChange={(event) => sethole1_4(event.target.value)}
                                value={hole1_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>2<span className={styles.spanpar}></span>{hole2_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole2_1"
                                type="number"
                                onChange={(event) => sethole2_1(event.target.value)}
                                value={hole2_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole2_2"
                                type="number"
                                onChange={(event) => sethole2_2(event.target.value)}
                                value={hole2_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole2_3"
                                type="number"
                                onChange={(event) => sethole2_3(event.target.value)}
                                value={hole2_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole2_4"
                                type="number"
                                onChange={(event) => sethole2_4(event.target.value)}
                                value={hole2_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>3<span className={styles.spanpar}></span>{hole3_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole3_1"
                                type="number"
                                onChange={(event) => sethole3_1(event.target.value)}
                                value={hole3_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole3_2"
                                type="number"
                                onChange={(event) => sethole3_2(event.target.value)}
                                value={hole3_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole3_3"
                                type="number"
                                onChange={(event) => sethole3_3(event.target.value)}
                                value={hole3_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole3_4"
                                type="number"
                                onChange={(event) => sethole3_4(event.target.value)}
                                value={hole3_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>4<span className={styles.spanpar}></span>{hole4_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole4_1"
                                type="number"
                                onChange={(event) => sethole4_1(event.target.value)}
                                value={hole4_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole4_2"
                                type="number"
                                onChange={(event) => sethole4_2(event.target.value)}
                                value={hole4_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole4_3"
                                type="number"
                                onChange={(event) => sethole4_3(event.target.value)}
                                value={hole4_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole4_4"
                                type="number"
                                onChange={(event) => sethole4_4(event.target.value)}
                                value={hole4_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>5<span className={styles.spanpar}></span>{hole5_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole15_1"
                                type="number"
                                onChange={(event) => sethole5_1(event.target.value)}
                                value={hole5_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole5_2"
                                type="number"
                                onChange={(event) => sethole5_2(event.target.value)}
                                value={hole5_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole5_3"
                                type="number"
                                onChange={(event) => sethole5_3(event.target.value)}
                                value={hole5_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole5_4"
                                type="number"
                                onChange={(event) => sethole5_4(event.target.value)}
                                value={hole5_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>6<span className={styles.spanpar}></span>{hole6_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole6_1"
                                type="number"
                                onChange={(event) => sethole6_1(event.target.value)}
                                value={hole6_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole6_2"
                                type="number"
                                onChange={(event) => sethole6_2(event.target.value)}
                                value={hole6_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole6_3"
                                type="number"
                                onChange={(event) => sethole6_3(event.target.value)}
                                value={hole6_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole6_4"
                                type="number"
                                onChange={(event) => sethole6_4(event.target.value)}
                                value={hole6_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>7<span className={styles.spanpar}></span>{hole7_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole7_1"
                                type="number"
                                onChange={(event) => sethole7_1(event.target.value)}
                                value={hole7_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole7_2"
                                type="number"
                                onChange={(event) => sethole7_2(event.target.value)}
                                value={hole7_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole7_3"
                                type="number"
                                onChange={(event) => sethole7_3(event.target.value)}
                                value={hole7_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole7_4"
                                type="number"
                                onChange={(event) => sethole7_4(event.target.value)}
                                value={hole7_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>8<span className={styles.spanpar}></span>{hole8_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole8_1"
                                type="number"
                                onChange={(event) => sethole8_1(event.target.value)}
                                value={hole8_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole8_2"
                                type="number"
                                onChange={(event) => sethole8_2(event.target.value)}
                                value={hole8_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole8_3"
                                type="number"
                                onChange={(event) => sethole8_3(event.target.value)}
                                value={hole8_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole8_4"
                                type="number"
                                onChange={(event) => sethole8_4(event.target.value)}
                                value={hole8_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number9}>9<span className={styles.spanpar}></span>{hole9_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole9_1"
                                type="number"
                                onChange={(event) => sethole9_1(event.target.value)}
                                value={hole9_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole9_2"
                                type="number"
                                onChange={(event) => sethole9_2(event.target.value)}
                                value={hole9_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole9_3"
                                type="number"
                                onChange={(event) => sethole9_3(event.target.value)}
                                value={hole9_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole9_4"
                                type="number"
                                onChange={(event) => sethole9_4(event.target.value)}
                                value={hole9_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.total_div}>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number_total}>Total F 9</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotal_1}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotal_2}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotal_3}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotal_4}</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
