import React, { useState, useEffect, useContext } from 'react'
import styles from '../styles/Scorecardcom.module.css'
import { GameContext }  from '../context/GameInfoForContext'
import { ParYardsContext }  from '../context/ParYardsForContext'

export default function Scorecardcom2() {

    const {course, player1, player2, player3, player4, 
        holeTotalb_1, setholeTotalb_1, holeTotalb_2, setholeTotalb_2,
        holeTotalb_3, setholeTotalb_3, holeTotalb_4, setholeTotalb_4} = useContext(GameContext);

    const {hole10_p, hole10_y, 
        hole11_p, hole11_y, hole12_p, hole12_y,
        hole13_y, hole13_p, hole14_y, hole14_p,
        hole15_y, hole15_p, hole16_y, hole16_p,
        hole17_y, hole17_p, hole18_p, hole18_y,} = useContext(ParYardsContext);    

    const [hole10_1, sethole10_1] = useState(null)
    const [hole10_2, sethole10_2] = useState(null)  
    const [hole10_3, sethole10_3] = useState(null) 
    const [hole10_4, sethole10_4] = useState(null) 

    const [hole11_1, sethole11_1] = useState(null)
    const [hole11_2, sethole11_2] = useState(null) 
    const [hole11_3, sethole11_3] = useState(null)
    const [hole11_4, sethole11_4] = useState(null)

    const [hole12_1, sethole12_1] = useState(null)
    const [hole12_2, sethole12_2] = useState(null)  
    const [hole12_3, sethole12_3] = useState(null) 
    const [hole12_4, sethole12_4] = useState(null) 

    const [hole13_1, sethole13_1] = useState(null)
    const [hole13_2, sethole13_2] = useState(null)  
    const [hole13_3, sethole13_3] = useState(null) 
    const [hole13_4, sethole13_4] = useState(null) 

    const [hole14_1, sethole14_1] = useState(null)
    const [hole14_2, sethole14_2] = useState(null)  
    const [hole14_3, sethole14_3] = useState(null) 
    const [hole14_4, sethole14_4] = useState(null) 

    const [hole15_1, sethole15_1] = useState(null)
    const [hole15_2, sethole15_2] = useState(null)  
    const [hole15_3, sethole15_3] = useState(null) 
    const [hole15_4, sethole15_4] = useState(null) 

    const [hole16_1, sethole16_1] = useState(null)
    const [hole16_2, sethole16_2] = useState(null)  
    const [hole16_3, sethole16_3] = useState(null) 
    const [hole16_4, sethole16_4] = useState(null) 

    const [hole17_1, sethole17_1] = useState(null)
    const [hole17_2, sethole17_2] = useState(null)  
    const [hole17_3, sethole17_3] = useState(null) 
    const [hole17_4, sethole17_4] = useState(null) 

    const [hole18_1, sethole18_1] = useState(null)
    const [hole18_2, sethole18_2] = useState(null)  
    const [hole18_3, sethole18_3] = useState(null) 
    const [hole18_4, sethole18_4] = useState(null) 

    // const [holeTotalb_1, setholeTotalb_1] = useState(null)
    // const [holeTotalb_2, setholeTotalb_2] = useState(null)  
    // const [holeTotalb_3, setholeTotalb_3] = useState(null) 
    // const [holeTotalb_4, setholeTotalb_4] = useState(null) 

    var totalStrokes_1 = {hole10_1, hole11_1, hole12_1, hole13_1, hole14_1, hole15_1, hole16_1, hole17_1, hole18_1}
    var totalStrokes_2 = {hole10_2, hole11_2, hole12_2, hole13_2, hole14_2, hole15_2, hole16_2, hole17_2, hole18_2}
    var totalStrokes_3 = {hole10_3, hole11_3, hole12_3, hole13_3, hole14_3, hole15_3, hole16_3, hole17_3, hole18_3}
    var totalStrokes_4 = {hole10_4, hole11_4, hole12_4, hole13_4, hole14_4, hole15_4, hole16_4, hole17_4, hole18_4}

     


    useEffect(() => {
        setholeTotalb_1(Number(hole10_1) + Number(hole11_1) + Number(hole12_1) + Number(hole13_1) + Number(hole14_1) + Number(hole15_1) + Number(hole16_1) + Number(hole17_1) + Number(hole18_1));
    }, [hole10_1, hole11_1, hole12_1, hole13_1, hole14_1, hole15_1, hole16_1, hole17_1, hole18_1, setholeTotalb_1])

    useEffect(() => {
        setholeTotalb_2(Number(hole10_2) + Number(hole11_2) + Number(hole12_2) + Number(hole13_2) + Number(hole14_2) + Number(hole15_2) + Number(hole16_2) + Number(hole17_2) + Number(hole18_2));
    }, [hole10_2, hole11_2, hole12_2, hole13_2, hole14_2, hole15_2, hole16_2, hole17_2, hole18_2, setholeTotalb_2])

    useEffect(() => {
        setholeTotalb_3(Number(hole10_3) + Number(hole11_3) + Number(hole12_3) + Number(hole13_3) + Number(hole14_3) + Number(hole15_3) + Number(hole16_3) + Number(hole17_3) + Number(hole18_3));
    }, [hole10_3, hole11_3, hole12_3, hole13_3, hole14_3, hole15_3, hole16_3, hole17_3, hole18_3, setholeTotalb_3])

    useEffect(() => {
        setholeTotalb_4(Number(hole10_4) + Number(hole11_4) + Number(hole12_4) + Number(hole13_4) + Number(hole14_4) + Number(hole15_4) + Number(hole16_4) + Number(hole17_4) + Number(hole18_4));
    }, [hole10_4, hole11_4, hole12_4, hole13_4, hole14_4, hole15_4, hole16_4, hole17_4, hole18_4, setholeTotalb_4])
// jscharlach@hotmail.com

    return (
        <div>
            <div className={styles.course_name_wrapper}>
            <h2 className={styles.course_name}>{course}</h2>
            <h2 className={styles.front_back}>Back Nine</h2>
            </div> 
            <div className={styles.curvedbit}></div>
            <div className={styles.curvedbit2}></div>
            <div className={styles.innercard_wrapper}>
                <div className={styles.innercard}>
                    <div className={styles.hole_name}>
                        <div className={styles.row}>
                        <div className={styles.col}>
                                <div className={styles.row}>
                                    <div className={styles.col2}>
                                        <p>H</p>&nbsp;&nbsp;
                                    </div>    
                                    <div className={styles.col2}>   
                                        <span className={styles.par}>Par</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player1}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player2}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player3}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.playesN}>{player4}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number1}>10<span></span>{hole10_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole10_1"
                                type="number"
                                onChange={(event) => sethole10_1(event.target.value)}
                                value={hole10_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole10_2"
                                type="number"
                                onChange={(event) => sethole10_2(event.target.value)}
                                value={hole10_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole10_3"
                                type="number"
                                onChange={(event) => sethole10_3(event.target.value)}
                                value={hole10_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole10_4"
                                type="number"
                                onChange={(event) => sethole10_4(event.target.value)}
                                value={hole10_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>11<span></span>{hole11_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole11_1"
                                type="number"
                                onChange={(event) => sethole11_1(event.target.value)}
                                value={hole11_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole11_2"
                                type="number"
                                onChange={(event) => sethole11_2(event.target.value)}
                                value={hole11_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole11_3"
                                type="number"
                                onChange={(event) => sethole11_3(event.target.value)}
                                value={hole11_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole11_4"
                                type="number"
                                onChange={(event) => sethole11_4(event.target.value)}
                                value={hole11_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>12<span></span>{hole12_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole12_1"
                                type="number"
                                onChange={(event) => sethole12_1(event.target.value)}
                                value={hole12_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole12_2"
                                type="number"
                                onChange={(event) => sethole12_2(event.target.value)}
                                value={hole12_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole12_3"
                                type="number"
                                onChange={(event) => sethole12_3(event.target.value)}
                                value={hole12_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole12_4"
                                type="number"
                                onChange={(event) => sethole12_4(event.target.value)}
                                value={hole12_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>13<span></span>{hole13_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole13_1"
                                type="number"
                                onChange={(event) => sethole13_1(event.target.value)}
                                value={hole13_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole13_2"
                                type="number"
                                onChange={(event) => sethole13_2(event.target.value)}
                                value={hole13_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole13_3"
                                type="number"
                                onChange={(event) => sethole13_3(event.target.value)}
                                value={hole13_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole13_4"
                                type="number"
                                onChange={(event) => sethole13_4(event.target.value)}
                                value={hole13_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>14<span></span>{hole14_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole14_1"
                                type="number"
                                onChange={(event) => sethole14_1(event.target.value)}
                                value={hole14_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole14_2"
                                type="number"
                                onChange={(event) => sethole14_2(event.target.value)}
                                value={hole14_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole14_3"
                                type="number"
                                onChange={(event) => sethole14_3(event.target.value)}
                                value={hole14_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole14_4"
                                type="number"
                                onChange={(event) => sethole14_4(event.target.value)}
                                value={hole14_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>15<span></span>{hole15_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole15_1"
                                type="number"
                                onChange={(event) => sethole15_1(event.target.value)}
                                value={hole15_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole15_2"
                                type="number"
                                onChange={(event) => sethole15_2(event.target.value)}
                                value={hole15_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole15_3"
                                type="number"
                                onChange={(event) => sethole15_3(event.target.value)}
                                value={hole15_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole15_4"
                                type="number"
                                onChange={(event) => sethole15_4(event.target.value)}
                                value={hole15_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>16<span></span>{hole16_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole16_1"
                                type="number"
                                onChange={(event) => sethole16_1(event.target.value)}
                                value={hole16_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole16_2"
                                type="number"
                                onChange={(event) => sethole16_2(event.target.value)}
                                value={hole16_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole16_3"
                                type="number"
                                onChange={(event) => sethole16_3(event.target.value)}
                                value={hole16_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole16_4"
                                type="number"
                                onChange={(event) => sethole16_4(event.target.value)}
                                value={hole16_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number}>17<span></span>{hole17_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole17_1"
                                type="number"
                                onChange={(event) => sethole17_1(event.target.value)}
                                value={hole17_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole17_2"
                                type="number"
                                onChange={(event) => sethole17_2(event.target.value)}
                                value={hole17_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole17_3"
                                type="number"
                                onChange={(event) => sethole17_3(event.target.value)}
                                value={hole17_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole17_4"
                                type="number"
                                onChange={(event) => sethole17_4(event.target.value)}
                                value={hole17_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number9}>18<span></span>{hole18_p}</p>
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole18_1"
                                type="number"
                                onChange={(event) => sethole18_1(event.target.value)}
                                value={hole18_1}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole18_2"
                                type="number"
                                onChange={(event) => sethole18_2(event.target.value)}
                                value={hole18_2}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole18_3"
                                type="number"
                                onChange={(event) => sethole18_3(event.target.value)}
                                value={hole18_3}
                                className={styles.hole_input}
                                />
                            </div>
                            <div className={styles.col}>
                                <input
                                name="hole18_4"
                                type="number"
                                onChange={(event) => sethole18_4(event.target.value)}
                                value={hole18_4}
                                className={styles.hole_input}
                                />
                            </div>
                        </div>
                        <div className={styles.total_div}>
                        <div className={styles.row}>
                            <div className={styles.col}>
                                <p className={styles.hole_number_total}>Total B 9</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotalb_1}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotalb_2}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotalb_3}</p>
                            </div>
                            <div className={styles.col}>
                                <p className={styles.total_score}>{holeTotalb_4}</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}