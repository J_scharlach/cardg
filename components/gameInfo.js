import React, { useContext, useState } from 'react'
import styles from '../styles/gameInfo.module.css'
import DatePicker from "react-datepicker";
import { GameContext } from '../context/GameInfoForContext'

import "react-datepicker/dist/react-datepicker.css";


export default function GameInfo() {

    const {email, setEmail, course, setCourse} = useContext(GameContext);

    const [startDate, setStartDate] = useState(new Date());


      function handleChange1(e) {
          setEmail(e.target.value);
      }

      function handleChange2(e) {
        setCourse(e.target.value);
    }
    return (
        <div className={styles.gameInfo}>
            <div className={styles.gameInfo_wrapper}>
                <h2 className={styles.gameInfo_name}>Todays Details</h2>
            </div> 
            <div className={styles.curvedbit}></div>
            <div className={styles.curvedbit2}></div>
            <div className={styles.formInfo}>
                <div>
                <input
                name="email"
                type="email"
                placeholder="Email"
                onChange={handleChange1}
                value={email}
                required
                className={styles.gameInfo_input}
                />
                </div>
                <div>
                <input
                name="course"
                type="text"
                placeholder="Course"
                onChange={handleChange2}
                value={course}
                required
                className={styles.gameInfo_input}
                />
                </div>
                <div>
                <DatePicker 
                className={styles.gameInfo_input} 
                required 
                selected={startDate} 
                value={startDate}
                onChange={(date) => setStartDate(date)} />
                {/* <button type="submit" className={styles.gameInfo_button}>Enter</button> */}
                </div>
            </div>
            {/* <p>{startDate}</p> */}
        </div>
    )
}


{/* <form onSubmit={handleSubmit} className={styles.gameInfo}>    */}