const withPWA = require('next-pwa')

// module.exports = withPWA({
//   reactStrictMode: true,
// })

module.exports = withPWA({
  pwa: {
    dest: 'public',
    // reactStrictMode: true,
  }
})