import '../styles/globals.css'
import GameProvider from '../context/GameInfoForContext'
import ParYardsProvider from '../context/ParYardsForContext'
import Footer from '../components/Footer'

function MyApp({ Component, pageProps }) {
  
  return (
      <GameProvider>
        <ParYardsProvider>
          <Component {...pageProps} />
          <Footer />
        </ParYardsProvider>
      </GameProvider>

  );
}

export default MyApp
