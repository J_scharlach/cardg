import React, {useContext, useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import logoTrans from '../public/logotrans.png'
import styles from '../styles/Home.module.css'
import { GameContext } from '../context/GameInfoForContext'

// var usePlayerContext = React.createContext(null);

export default function Landing() {

  const {player1, player2, player3, player4,
        slope_1, setSlope1, rate_1, setRate1, slope_2, setSlope2, rate_2, setRate2,
        slope_3, setSlope3, rate_3, setRate3, slope_4, setSlope4, rate_4, setRate4,
        index1, setindex1, index2, setindex2, index3, setindex3, index4, setindex4,
        par1, setpar1, par2, setpar2, par3, setpar3, par4, setpar4
        } = useContext(GameContext);


  return (
    <div>
      <Image layout="fixed" width={281} height={203} src={logoTrans} alt="logo image" />
      <div className={styles.complete_div}>
        <div className={styles.landing_info}>
            <p className={styles.landing_discrp}>Your Course Handicap Calculator</p>
        </div>
      <div className={styles.scorecard_div}>
        <div className={styles.scorecard}>
          <div className={styles.course_name_wrapper}>
            <h2 className={styles.course_name}>Players</h2>
          </div> 
          <div className={styles.curvedbit}></div>
          <div className={styles.curvedbit2}></div>
          <div className={styles.innercard_wrapper}>
            <div className={styles.innercard}>
                <div className={styles.hole_name}>
                    <div className={styles.row}>
                    </div>
                    <div>
                      <form className={styles.gameInfo}>
                      {/* <form onSubmit={handleSubmit} className={styles.gameInfo}>    */}
                          <p className={styles.handiPlayer}>Player: {player1}</p>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Slope</p>
                              <input
                                  name="slope_1"
                                  type="number"
                                  onChange={(event) => setSlope1(event.target.value)}
                                  value={slope_1}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Rating</p>
                              <input
                                  name="rate_1"
                                  type="number"
                                  onChange={(event) => setRate1(event.target.value)}
                                  value={rate_1}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Handicap Index</p>
                              <input
                                  name="index1"
                                  type="number"
                                  onChange={(event) => setindex1(event.target.value)}
                                  value={index1}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Par</p>
                              <input
                                  name="par1"
                                  type="number"
                                  onChange={(event) => setpar1(event.target.value)}
                                  value={par1}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <div>
                              <p>Course Handicap: {Math.floor(index1 * slope_1 / 113 + (rate_1 - par1))}</p>
                              </div>
                            </div> 
                          </div>
                          <p className={styles.handiPlayer}>Player: {player2}</p>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Slope</p>
                              <input
                                  name="slope_2"
                                  type="number"
                                  onChange={(event) => setSlope2(event.target.value)}
                                  value={slope_2}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Rating</p>
                              <input
                                  name="rate_2"
                                  type="number"
                                  onChange={(event) => setRate2(event.target.value)}
                                  value={rate_2}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Handicap Index</p>
                              <input
                                  name="index2"
                                  type="number"
                                  onChange={(event) => setindex2(event.target.value)}
                                  value={index2}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Par</p>
                              <input
                                  name="par2"
                                  type="number"
                                  onChange={(event) => setpar2(event.target.value)}
                                  value={par2}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <div>
                              <p>Course Handicap: {Math.floor(index2 * slope_2 / 113 + (rate_2 - par2))}</p>
                              </div>
                            </div> 
                          </div>
                          <p className={styles.handiPlayer}>Player: {player3}</p>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Slope</p>
                              <input
                                  name="slope_3"
                                  type="number"
                                  onChange={(event) => setSlope3(event.target.value)}
                                  value={slope_3}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Rating</p>
                              <input
                                  name="rate_3"
                                  type="number"
                                  onChange={(event) => setRate3(event.target.value)}
                                  value={rate_3}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Handicap Index</p>
                              <input
                                  name="index3"
                                  type="number"
                                  onChange={(event) => setindex3(event.target.value)}
                                  value={index3}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Par</p>
                              <input
                                  name="par3"
                                  type="number"
                                  onChange={(event) => setpar3(event.target.value)}
                                  value={par3}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <div>
                              <p>Course Handicap: {Math.floor(index3 * slope_3 / 113 + (rate_3 - par3))}</p>
                              </div>
                            </div> 
                          </div>
                          <p className={styles.handiPlayer}>Player: {player4}</p>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Slope</p>
                              <input
                                  name="slope_4"
                                  type="number"
                                  onChange={(event) => setSlope4(event.target.value)}
                                  value={slope_4}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Course Tee Rating</p>
                              <input
                                  name="rate_4"
                                  type="number"
                                  onChange={(event) => setRate4(event.target.value)}
                                  value={rate_4}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Handicap Index</p>
                              <input
                                  name="index4"
                                  type="number"
                                  onChange={(event) => setindex4(event.target.value)}
                                  value={index4}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <p>Par</p>
                              <input
                                  name="par4"
                                  type="number"
                                  onChange={(event) => setpar4(event.target.value)}
                                  value={par4}
                                  className={styles.hole_input}
                                  />
                            </div>      
                          </div>
                          <div className={styles.row}>
                            <div className={styles.col}>
                              <div>
                              <p>Course Handicap: {Math.floor(index4 * slope_4 / 113 + (rate_4 - par4))}</p>
                              </div>
                            </div> 
                          </div>
                      </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      {/* <p className={styles.handiPlayer}>If you don't have the course information on hand - use the <a href="https://www.randa.org/worldhandicapsystem/Lookup" className={styles.lookUp}>'Look up'</a> option.</p> */}
      <div className={styles.parYards_div}></div>
        <div>
            <Link href='/scorecard' passHref>
              <button type="submit" className={styles.gameInfo_button}>Scorecard</button>
            </Link>
        </div>
        <div>
            <Link href='/info' passHref>
                <button type="submit" className={styles.gameInfo_button}>Home</button>
            </Link>
        </div>
      <div>
      </div>
      </div>
    </div>
  )
}
