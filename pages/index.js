import React from 'react'
import Image from 'next/image'
import logoTrans from '../public/logotrans.png'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';

export default function Home() {

  return (
    <div>
      <Head>
        <title>Caddy Card</title>
        <meta name="description" content="Golfing scorecard and more" />

        <meta name='application-name' content='Caddy Card' />
        <meta name='apple-mobile-web-app-capable' content='yes' />
        <meta name='apple-mobile-web-app-status-bar-style' content='default' />
        <meta name='apple-mobile-web-app-title' content='Caddy Card' />
        <meta name='description' content='Golf score card and more' />
        <meta name='format-detection' content='telephone=no' />
        <meta name='mobile-web-app-capable' content='yes' />
        <meta name='msapplication-config' content='/static/icons/browserconfig.xml' />
        <meta name='msapplication-TileColor' content='#bfed91' />
        <meta name='msapplication-tap-highlight' content='no' />
        <meta name='theme-color' content='#000000' />
        <link rel="icon" href="/static/icons/favicon.ico" />

        <link rel='icon' type='image/png' sizes='32x32' href='/public/flavicon-32x32.png' />
        <link rel='icon' type='image/png' sizes='16x16' href='/public/flavicon-16x16.png' />
        <link rel='manifest' href='/static/manifest.json' />
        <link rel='mask-icon' href='/static/icons/safari-pinned-tab.svg' color='#98f092' />
        <link rel='shortcut icon' href='/public/favicon.ico' />
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500' />

        {/* <link href="https://fonts.googleapis.com/css2?family=Nova+Round&display=swap" rel="stylesheet"></link> */}
      </Head>
      <div className={styles.landing_div}>
        <div className={styles.landing_main_div}>
          <Zoom duration={3500}>
            <Image layout="fixed" width={281} height={203} src={logoTrans} alt="logo image" />
          </Zoom>
          <Fade left duration={3500} delay={3500}> 
            <div className={styles.landing_info}>
                <p className={styles.landing_discrp}>Your Digital Golf Assistant</p>
            </div>
          </Fade> 
          <Zoom duration={1500} delay={1000} >
            <div>
              <Link href='/info' passHref>
                  <button type="submit" className={styles.gameInfo_button}>Tee it up</button>
              </Link>
            </div> 
          </Zoom>
        </div>
      </div>
    </div>   
  )
}