import React, {useContext, useState, useEffect} from 'react'
import Image from 'next/image'
import Link from 'next/link'
import logoTrans from '../public/logotrans.png'
import styles from '../styles/Home.module.css'
import GameInfo from '../components/gameInfo'
import ParYards from '../components/parYards'
import ParYardsBack from '../components/parYardsBack'
import { GameContext } from '../context/GameInfoForContext'

// var usePlayerContext = React.createContext(null);

export default function Landing() {

  const {player1, setplayer1, player2, setplayer2, 
        player3, setplayer3, player4, setplayer4
        } = useContext(GameContext);

  const [author, setAuthor] = useState('')       
  const [quote, setQuote] = useState('')

  useEffect(() =>{
      fetch("https://type.fit/api/quotes")
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      let number = Math.floor(Math.random() * 1600);

      setAuthor(data[number].author);

      setQuote(data[number].text); 
    });
  }, [])
  
       
 
  return (
    <div>
      <Image layout="fixed" width={281} height={203} src={logoTrans} alt="logo image" />
      <div className={styles.complete_div}>
      <div className={styles.gameInfo_div}>
        <GameInfo />
      </div >
      <div className={styles.scorecard_div}>
        <div className={styles.scorecard}>
          <div className={styles.course_name_wrapper}>
            <h2 className={styles.course_name}>Players</h2>
          </div> 
          <div className={styles.curvedbit}></div>
          <div className={styles.curvedbit2}></div>
          <div className={styles.innercard_wrapper}>
            <div className={styles.innercard}>
                <div className={styles.hole_name}>
                    <div className={styles.row}>
                        <p className={styles.players_title}>Enter Player Names</p>
                    </div>
                    <div>
                      <form className={styles.gameInfo}>
                      {/* <form onSubmit={handleSubmit} className={styles.gameInfo}>    */}
                          <input
                          name="player1"
                          placeholder="Player 1"
                          onChange={(event) => setplayer1(event.target.value)}
                          value={player1}
                          className={styles.gameInfo_input}
                          />
                          <input
                          name="player2"
                          type="text"
                          placeholder="Player 2"
                          onChange={(event) => setplayer2(event.target.value)}
                          value={player2}
                          className={styles.gameInfo_input}
                          />
                          <input
                          name="player3"
                          type="text"
                          placeholder="Player 3"
                          onChange={(event) => setplayer3(event.target.value)}
                          value={player3}
                          className={styles.gameInfo_input}
                          />
                          <input
                          name="player 4"
                          type="text"
                          placeholder="Player4"
                          onChange={(event) => setplayer4(event.target.value)}
                          value={player4}
                          className={styles.gameInfo_input}
                          />
                      </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <ParYards />
      <div className={styles.parYards_div}></div>
        <ParYardsBack />
        <div>
            <Link href='/scorecard' passHref>
              <button type="submit" className={styles.gameInfo_button4}>Scorecard</button>
            </Link>
        </div>
        <div>
            <Link href='/courseHandicapCalculator' passHref>
              <button type="submit" className={styles.gameInfo_button2}>Course Handicap Calculator</button>
            </Link>
        </div>
      <div className={styles.quote_top_div}>
      <div className={styles.quote_div}>
        <p className={styles.quote_title}>Random inspirational Quotes to get your game started</p>
        <p className={styles.quote_quote}>{quote}</p>
        <p className={styles.quote_author}>{author}</p>
      </div>
      </div>
      <div>

      </div>
      </div>
    </div>
  )
}
