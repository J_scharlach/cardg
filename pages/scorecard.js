import React, { useContext, useState, useEffect } from 'react'
import styles from '../styles/Scorecard.module.css'
import Link from 'next/link'
import Scorecardcom from '../components/scorecardcom'
import Scorecardcom2 from '../components/scorecardcom2'
import { GameContext }  from '../context/GameInfoForContext'


export default function Scorecard() {

    const {course, player1, player2, player3, player4, 
        holeTotal_1, holeTotal_2,
        holeTotal_3,  holeTotal_4,
        holeTotalb_1, holeTotalb_2,
        holeTotalb_3, holeTotalb_4, 
    } = useContext(GameContext);

    const [courseTotal_1, setcourseTotal_1] = useState()
    const [courseTotal_2, setcourseTotal_2] = useState()
    const [courseTotal_3, setcourseTotal_3] = useState()
    const [courseTotal_4, setcourseTotal_4] = useState()

    useEffect(() => {
        setcourseTotal_1(Number(holeTotal_1) + Number(holeTotalb_1));
    }, [holeTotal_1, holeTotalb_1])

    useEffect(() => {
        setcourseTotal_2(Number(holeTotal_2) + Number(holeTotalb_2));
    }, [holeTotal_2, holeTotalb_2])

    useEffect(() => {
        setcourseTotal_3(Number(holeTotal_3) + Number(holeTotalb_3));
    }, [holeTotal_3, holeTotalb_3])

    useEffect(() => {
        setcourseTotal_4(Number(holeTotal_4) + Number(holeTotalb_4));
    }, [holeTotal_4, holeTotalb_4])
    

    return (
        <div>
            <main className={styles.main}>
                <h1 className={styles.title}>
                Score Card
                </h1>
            </main>
            <div className={styles.scorecard_div}>
                <div className={styles.scorecard}>
                <Scorecardcom  />
                </div>
            </div>
            <div className={styles.scorecard_div}>
                <div className={styles.scorecard}>
                <Scorecardcom2  />
                </div>
            </div>
            <div>
            <div className={styles.scorecard_div}>
                <div className={styles.scorecard}>   
                    <div className={styles.course_name_wrapper}>
                    <h2 className={styles.course_name}>{course}</h2>
                    <h2 className={styles.front_back}>Total Score</h2>
                    </div> 
                    <div className={styles.curvedbit}></div>
                    <div className={styles.curvedbit2}></div>
                    <div className={styles.innercard_wrapper}>
                        <div className={styles.innercard}>
                        <div className={styles.total_div}>
                            <div className={styles.hole_name}>
                                <div className={styles.row}>
                                    <div className={styles.col}>
                                        <p>H</p>
                                    </div>
                                    <div className={styles.col}>
                                        <p className={styles.playesN}>{player1}</p>
                                    </div>
                                    <div className={styles.col}>
                                        <p className={styles.playesN}>{player2}</p>
                                    </div>
                                    <div className={styles.col}>
                                        <p className={styles.playesN}>{player3}</p>
                                    </div>
                                    <div className={styles.col}>
                                        <p className={styles.playesN}>{player4}</p>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.row}>
                                <div className={styles.col}>
                                    <p className={styles.hole_number_total}>Total F 9</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotal_1}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotal_2}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotal_3}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotal_4}</p>
                                </div>
                            </div>
                            <div className={styles.row}>
                                <div className={styles.col}>
                                    <p className={styles.hole_number_total}>Total B 9</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotalb_1}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotalb_2}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotalb_3}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{holeTotalb_4}</p>
                                </div>
                            </div>
                            <div className={styles.row_total}>
                                <div className={styles.col}>
                                    <p className={styles.hole_number_total}>Total</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{courseTotal_1}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{courseTotal_2}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{courseTotal_3}</p>
                                </div>
                                <div className={styles.col}>
                                    <p className={styles.total_score}>{courseTotal_4}</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
            <Link href='/info' passHref>
                <button className={styles.returnHome_button}>Home</button>
            </Link>
        <div>
            <Link href='/courseHandicapCalculator' passHref>
              <button type="submit" className={styles.gameInfo_button2}>Course Handicap Calculator</button>
            </Link>
        </div>    
    </div>
    )
}
